Author of the `ekdosis` package
===============================

Robert Alessi
: Personal email: <alessi@robertalessi.net>
: Institutional email: <robert.alessi@cnrs.fr>
: Affiliation: [UMR 8167 Orient & Méditerranée (Paris, France)](https://www.orient-mediterranee.com)
: More information: <http://www.ekdosis.org/about.html> or <https://ctan.org/pkg/ekdosis>
